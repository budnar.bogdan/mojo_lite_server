steps:
1 instaleaza strawberryperl de pe site-ul asta http://strawberryperl.com
2 instaleaza git https://git-scm.com/download/win si cloneazatiti repo-ul asta : https://gitlab.com/budnar.bogdan/mojo_lite_server
3 instaleaza mysql de pe : https://dev.mysql.com/downloads/mysql/ si workbench de pe https://www.mysql.com/products/workbench/ seteaza parola la userul root 'password'
si creaza database-ul 'licenta'

start app :
mergi in folderul  mojo_lite_server si rulezi in consola de Perl : perl -I ./ myapp.pl daemon

rute :
post localhost:8080/web/initializeDB - pentru initializarea db-ului
post localhost:8080/web/login - pentru login trimite pe body username-ul si parola ex :
ex request :
{
	"username": "admin",
	"password": "123"
}
ex response :
{
    "success": 1
 }
{
    "error": "could not log in"
}
post localhost:8080/web/createUser - pentru creare de useri
ex request :
{
	"username": "admin",
	"password": "123",
	"email":"test@test.com"
}
ex response :
{
    "error": "User already exists"
}
{
    "success": 1
}
post localhost:8080/web/logout
