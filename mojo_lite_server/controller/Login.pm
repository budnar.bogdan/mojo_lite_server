package controller::Login;

use repo::User;

sub login {
    my $c = shift;

    my $params = $c->req->json;
    my $email = $params->{email};
    my $password = $params->{password};

    my $local_user = repo::User::getUser($email);
    return 0 unless $local_user;

    if ($email eq $local_user->{email} && $password eq $local_user->{password}){
        $c->session(auth => 1);
        $c->session(email => $email);
        return 1;
    }

    $c->flash('error' => 'Wrong login/password');
    return 0;
}

sub createUser {
    my $c = shift;
    my $params = $c->req->json;

    my $create = repo::User::createUser($params);

    if (!$create){
        return { error => "The user could not be created! Please try again!"};
    } elsif (length( $create ) > 1 ){
        return { error => $create};
    } else {
        return { success => 1};
    }
}

sub getAdmin {
    my $c = shift;
    my $params = $c->req->json;

    if ($params->{email} eq 'iulia@test.com' && $params->{password} eq 'Admin123'){
        my $users = repo::User::getUsers();
        return{ success => 1, users => $users };
    }
    return { error => 1 };
}

1;
