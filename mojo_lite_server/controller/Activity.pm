package controller::Activity;

use repo::Activity;

sub getActivity {
    my $c = shift;

    my $params = $c->req->json;
    my $email = $c->session('email');
    my $local_user = repo::User::getUser($email);

    my $activities = repo::Activity::getActivity($local_user->{id});

    return { success => 1, activities => $activities };
}

sub addActivity {
    my $c = shift;

    my $params = $c->req->json;

    my $email = $c->session('email');
    my $local_user = repo::User::getUser($email);

    my $activity = {
        name => $params->{sport},
        time => $params->{time},
        date => $params->{date}
    };

    return { error => "The activity name is missing!"} unless $activity->{name};
    return { error => "The time is missing!"} unless $activity->{time};
    return { error => "The date is missing!"} unless $activity->{date};

    repo::Activity::addActivity($local_user->{id}, $activity);

    return { success => 1 };
}

1;
