package repo::Activity;

use utils::ConnectDB qw(getDBH);

sub getActivity {
    my $user_id = shift;

    my $sql = "select name,time, date from `licenta`.`activity` as ac inner join `licenta`.`user` as us on us.id = ac.id_user WHERE us.id = ?";
    my $dbh = getDBH();
    my $sth = $dbh->prepare($sql);
    $sth->execute($user_id);
    my @activities;

    while (my $activity = $sth->fetchrow_hashref){
        push @activities, $activity;
    }
    return \@activities;
}

sub addActivity{
    my ($user_id, $activity) = @_;

    my $sql = "INSERT INTO `licenta`.`activity` (`id_user`, `name`, `time`, `date`) VALUES (?, ?, ?, ?)";
    my $dbh = getDBH();
    $dbh->do($sql, undef, $user_id, $activity->{name}, $activity->{time}, $activity->{date});
}

1;
