package repo::User;

use utils::ConnectDB qw(getDBH);

sub getUser {
    my $email = shift;

    my $sql = 'SELECT * FROM user WHERE email = ? LIMIT 1';
    my $dbh = getDBH();
    my $sth = $dbh->prepare($sql);
    $sth->execute($email);
    my $user;
    while( my $row = $sth->fetchrow_hashref){
        $user = $row;
    }

    return $user;
}

sub createUser {
    my $params = shift;

    if (!$params->{password} || !$params->{name} || !$params->{email}){
        warn "You need to fill all fields";
        return "You need to fill all fields";
    }

    if(getUser($params->{email})){
        warn "User already exists";
        return "User already exists";
    }

    my $sql = 'INSERT INTO `licenta`.`user` (`email`, `username`, `password`) VALUES (?, ?, ?);';
    my $dbh = getDBH();

    return $dbh->do($sql, undef, $params->{email}, $params->{name}, $params->{password});
}

sub getUsers {
    my $sql = 'SELECT * FROM user';
    my $dbh = getDBH();
    my $sth = $dbh->prepare($sql);
    $sth->execute();
    my @users;
    while( my $row = $sth->fetchrow_hashref){
        push @users, $row;
    }

    return \@users;

}

1;
