package utils::ConnectDB;
use Exporter 'import';

@EXPORT_OK = qw(getDBH);

use strict;
use warnings;
use Data::Dumper;
use DBI;
use DBD::mysql;

my $db_conf = 'utils\db.conf';

sub getDBH {
    my $dbh;
    my $conf = {};
    open my $fh, '<', $db_conf;
    while (my $line = <$fh>){
        if ($line =~ /(.+)=(.+)/){
            $conf->{$1} = $2;
        }
    }
    close $fh;
    eval {
        my $dsn = sprintf("DBI:mysql:database=licenta:host=%s:port=3306", $conf->{host} || 'localhost');
        $dbh = DBI->connect($dsn, $conf->{user} || "root", $conf->{pass} || "password");
    };
    if ($@){
        warn "Some error : ".Dumper($@);
    }
    return $dbh;
}

1;
