package utils::Helper;

use strict;
use warnings;
use utils::ConnectDB;
use Data::Dumper;

sub initializeDB {
    my $dbh = utils::ConnectDB::getDBH();

    my $sql = "CREATE TABLE IF NOT EXISTS `licenta`.`user` (
                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                    `email` VARCHAR(50) NOT NULL,
                    `username` VARCHAR(50) NOT NULL,
                    `password` VARCHAR(255) NOT NULL,
                    PRIMARY KEY(`id`)
                )";
    $dbh->do($sql);
    $sql = "CREATE TABLE `licenta`.`activity` (
        `id_activity` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(255) NULL,
        `time` INT NULL,
        `date` DATETIME NULL,
        PRIMARY KEY (`id_activity`))";
    $dbh->do($sql);

    $sql = "ALTER TABLE `licenta`.`activity`
    ADD COLUMN `id_user` INT NULL AFTER `id_activity`,
    ADD INDEX `fkey_idx` (`id_user` ASC) VISIBLE;
    ;
    ALTER TABLE `licenta`.`activity`
    ADD CONSTRAINT `fkey`
    FOREIGN KEY (`id_user`)
    REFERENCES `licenta`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;";
    $dbh->do($sql);
}

1;
