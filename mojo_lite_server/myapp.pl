#!/usr/bin/env perl
use Mojolicious::Lite;
use utils::Helper;

# Documentation browser under "/perldoc"
plugin 'PODRenderer';

use controller::Login;
use controller::Activity;

hook before_dispatch => sub {
    my $c = shift;
    $c->res->headers->header('Access-Control-Allow-Origin' => '*');
};

get '/' => sub {
  my $c = shift;
  $c->render(template => 'index');
};

post '/web/initializeDB' => sub {
    my $c = shift;
    utils::Helper::initializeDB();
    $c->render(json => { success => 1 });
};

# options '/web/login' => sub {
# };

post '/web/login' => sub {
    my $c = shift;
    my $result = controller::Login::login($c);

    my $render = $result ? {success => 1 } : {error => "could not log in"};
    my $status = $render->{success} ? "200" : "401";

    $c->render(json => $render, status => $status);
};

post '/web/logout' => sub {
    my $c = shift;
    delete $c->session->{auth};
    delete $c->session->{username};

    $c->redirect_to('index');
};

post '/web/createUser' => sub {
    my $c = shift;
    my $response = controller::Login::createUser($c);
    my $status = $response->{success} ? "200" : "500";
    $c->render(json => $response, status => $status);
};

post '/web/getActivity' => sub {
    my $c = shift;
    my $response = controller::Activity::getActivity($c);
    $c->render(json => $response);
};

post '/web/addActivity' => sub {
    my $c = shift;
    my $response = controller::Activity::addActivity($c);
    $c->render(json => $response);
};

post '/web/getAdmin' => sub {
    my $c = shift;
    my $response = controller::Login::getAdmin($c);
    my $status = $response->{success} ? "200" : "401";

    $c->render(json => $response, status => $status);
};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>Welcome to the Mojolicious real-time web framework!</h1>
To learn more, you can browse through the documentation
<%= link_to 'here' => '/perldoc' %>.

@@ index2.html.ep
% layout 'default';
% title 'success';
<h1>you logged in</h1>
To learn more, you can browse through the documentation
<%= link_to 'here' => '/perldoc' %>.

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
